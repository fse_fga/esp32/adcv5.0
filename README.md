# Exemplo do uso do ADC na ESP32 com a ESP-IDF v5.0

Este exemplo demonstra o uso do ADC com a aquisição do tipo `one_shot`. O exemplo usa um Joystick XY com botão como exemplo.

Além do uso do ADC, o exemplo traz uma proposta de abstração da inicialização dos pinos digitais da GPIO e dos pinos do ADC de modo semelhante à biblioteca do Arduino.
