#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_adc/adc_oneshot.h"

#include "gpio_setup.h"
#include "adc_module.h"

#define JOYSTICK_X ADC_CHANNEL_3
#define JOYSTICK_Y ADC_CHANNEL_6
#define JOYSTICK_BOTAO 36

void app_main()
{
  adc_init(ADC_UNIT_1);

  pinMode(JOYSTICK_BOTAO, GPIO_INPUT_PULLDOWN);
  pinMode(JOYSTICK_X, GPIO_ANALOG);
  pinMode(JOYSTICK_Y, GPIO_ANALOG);

  while (true)
  {
    int posicao_x = analogRead(JOYSTICK_X);
    int posicao_y = analogRead(JOYSTICK_Y);
    int botao = digitalRead(JOYSTICK_BOTAO);

    posicao_x = posicao_x - 2048;
    posicao_y = posicao_y - 2048;
    printf("Posição X: %.3d \t Posição Y: %.3d | Botão: %d\n", posicao_x, posicao_y, botao);
    vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}